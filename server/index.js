const express = require('express')
const app = express()
const port = process.env.PORT || 3004;

app.get('/', (request, response) => {
    response.send('Hello from Express! Added some changes')
})

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
});


app.get('/',
    ( request,
      response)=>{
         response.json({
             success:true,
             data:[
                 {title: 'Some data'}
                ]
    })
})