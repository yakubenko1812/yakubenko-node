export default class CoreService {
    constructor() {
        // this.url = 'https://yakubenko-node-server.herokuapp.com/';
        this.url = 'http://localhost:3005/api/v1';
    }
    /**
     * @desc Execute all ajax requests
     * @param {String} url
     * @param {Object} options
     * @return {Promise}
     **/
    async request(url, options = {}) {
        return fetch(
            `${this.url}${url.charAt(0) !== '/'
                ? '/' + url
                : url
            }`,
            options
        )
            .then(response => response.json());
    }
    async getRequest(url) {
        return this.request(url);
    }
    async postRequest(url, data = {}) {
        console.log({
            method: 'POST',
            body: JSON.stringify(data)
        });
        return this.request(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        });
    }
    async putRequest(url, data = {}) {
        return this.request(url, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        });
    }
    async deleteRequest(url, data = {}) {
        return this.request(url, {
            method: 'DELETE',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        });
    }
}