import expressLoader from './express';

export default async function({config, app}) {
    await expressLoader({config, app});
    console.log('✌ Express loaded.');


}
