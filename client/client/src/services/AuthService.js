import CoreService from "./CoreService";

export default  class AuthService extends CoreService{
    constructor() {
        super();
    }

    async userSignUp({email, first_name, last_name}){
        return this.postRequest(
            '/auth/signup', {
                email,
                first_name,
                last_name
            })
    }
}