import {Router} from 'express';
import {
    SignInController,
    SignUpController
} from './controller';

const route = Router();

export default function(root) {
    root.use('/auth', route);

    /**
     * @desc POST-метод для створення користувача
     **/
    route.post(
        '/signup',
        SignUpController
    );
    //
    // /**
    //  * @desc POST-метод для авторизації користувача
    //  **/
    // route.post('/signin', SignInController);
}

