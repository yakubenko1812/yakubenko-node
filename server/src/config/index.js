import dotenv from 'dotenv';

const environment = dotenv.config();

if (!environment) {
    throw new Error('.env can not be found!');
}

export default {
    /**
     * @desc Порт на якому працює сервер
     * @type {Number}
     **/
    PORT: process.env.PORT,

    /**
     * @desc Префікс АПІ серверу
     * @type {String}
     **/
    SERVICE_PREFIX: process.env.SERVICE_PREFIX || 'api',

    /**
     * @desc Версія сервісу
     * @type {Number}
     **/
    API_VERSION: process.env.API_VERSION || 1
};
