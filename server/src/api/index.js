import {Router} from 'express';
import AuthRoute from './routes/auth';
import ProductRoute from './routes/products';
export default function() {
    const router = Router();
    AuthRoute(router);
    ProductRoute(router);
    return router;
}
