import '@babel/polyfill';
import 'regenerator-runtime/runtime.js';

import config from './config';
import loaders from './loaders';
import express from 'express';

StartServer()
    .catch(err => console.log('Err -> ', err));

/**
 * @desc Запуск Веб-сервера
 **/
async function StartServer() {
    const app = express();

    await loaders({config, app});

    app.listen(config.PORT, err => {
        if (err) {
            console.log('Error has just happened --> ', err);
            process.exit(1);
            return;
        }

        console.log(`
        ################################################
        🛡️ Server listening on port: ${config.PORT} and version: ${config.SERVICE_PREFIX}/v${config.API_VERSION}
        ################################################`);
    });
}

export default StartServer;
