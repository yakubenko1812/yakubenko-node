import UserSignUpForm from './SignUpForm';
import {AuthService} from '../../services';
import './UserSignUpPage.css';
export default function UserSignUpPage({}) {
    const onUserSignUpForm = async data => {
        await new AuthService()
            .userSignUp(data);
    }
    return (
        <div className='user-signup-page'>
            <UserSignUpForm onSubmit={onUserSignUpForm}/>
        </div>
    )
}