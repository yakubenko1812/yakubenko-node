/**
 * @desc
 **/
export const SignUpController = (request, response) => {
    const {
        password,
        first_name,
        last_name,
        email
    } = request.body;
    response.json({
        success: true,
        user: {
            password,
            first_name,
            last_name,
            email
        }
    });
}
/**
 * @desc
 **/
export const SignInController = () => {}