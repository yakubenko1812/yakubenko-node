/**
 * @desc Express Loader
 **/
import routes from '../api';
import cors from 'cors';
import bodyParser from 'body-parser';

const allowedOrigins = [
    'http://localhost:3000'
];

export default async function({config, app}) {
    const correctUrl = url => {
        return url && url.charAt(0) !== '/'
            ? `/${url}`
            : url
    }

    app.use(cors({
        origin: function (origin, callback) {
            // allow requests with no origin
            // (like mobile apps or curl requests)
            if (!origin) {
                return callback(null, true);
            }

            if (allowedOrigins.indexOf(origin) === -1) {
                const msg = 'The CORS policy for this site does not allow access from the specified Origin.';

                return callback(new Error(msg), false);
            }

            return callback(null, true);
        },
        credentials: true
    }));

    // Load API routes
    app.use(
        `${correctUrl(config.SERVICE_PREFIX)}/v${config.API_VERSION}`,
        routes()
    );
}
