import {useState} from 'react';
const initForm = {
    email: '',
    first_name: '',
    last_name: '',
    password: ''
};
export default function SignUpForm({onSubmit}) {
    const [data, setData] = useState(initForm);
    const onFormSubmit = event => {
        event.preventDefault();
        if (typeof onSubmit === 'function') {
            onSubmit(data)
        }
    }
    const onInputChange = ({target: {name, value}}) => {
        setData({
            ...data,
            [name]: value
        })
    }
    return (
        <form onSubmit={onFormSubmit}>
            <div className='form-item'>
                <label htmlFor='txtEmail'>Email:</label>
                <input id='txtEmail'
                       name='email'
                       type='email'
                       onChange={onInputChange}
                       value={data.email}/>
            </div>
            <div className='form-item'>
                <label htmlFor='txtFirstName'>First name:</label>
                <input id='txtFirstName'
                       name='first_name'
                       type='text'
                       onChange={onInputChange}
                       value={data.first_name}/>
            </div>
            <div className='form-item'>
                <label htmlFor='txtLastName'>First name:</label>
                <input id='txtLastName'
                       name='last_name'
                       type='text'
                       onChange={onInputChange}
                       value={data.last_name}/>
            </div>
            <div className='form-item'>
                <label htmlFor='txtPassword'>Password:</label>
                <input id='txtPassword'
                       name='password'
                       type='password'
                       onChange={onInputChange}
                       value={data.password}/>
            </div>
            <div className='form-item'>
                <button type='submit'>Submit</button>
            </div>
        </form>
    );
}