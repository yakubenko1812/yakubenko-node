
import './App.css';
import UserSignUpPage from "./components/UserSignUpPage";
function App() {
    const  onSendAjaxRequestClick = () => {
    }
    return (
        <div className="App">
            <h1>Testing</h1>
            <button onClick={onSendAjaxRequestClick}>Send AJAX request</button>
            <hr/>
            <br/>
            <UserSignUpPage />
        </div>
    );
}
export default App;